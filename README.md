# Identifying Tweets from the Economic Domain
This repository contains a data set used for classifying tweets as either originating from an economist or not.

## Overview
This section contains a short overview about the files used in the dataset.

### Economic Conferences
For the first approach a set of economic conferences was analysed to get an overview about the economic domain on Twitter. By using this non-representative selection of conference it turned out, that only a small percentage of conference is representet on twitter.

|Conference|Short Name|Webpage|Twitter Account|Hashtag|Remark|
|----------|----------|-------|---------------|-------|------|
|Academy of Business and Retail Management / International Academic Conference in Rome|||ABRMCom||institutional account
|Advances in Micro econometrics ||http://www.cemmap.ac.uk/event/id/878|||
|AEA Annual Meeting|ASSA |http://www.aeaweb.org/Annual_Meeting/index.php|||
|AFA Annual Meeting||http://www.afajof.org/view/index.html|JofFinance||institutional account
|Annual Conference on the Political Economy of International Organizations |peio|http://www.peio.me/|||
|Annual Congress of the European Accounting Association |eaa2014|http://www.eaa2014.org||#eaa2014|bad snr with hashtag|

The full list can be found [here](conferences.csv)

### Twitter Accounts
A list of all Accounts which as been processed in this paper can be found [here](seeds.csv). The following section gives an short overview about the data stored there:

|Twitter Userid|Type|
|--------------|----|
|231253807|FOLLOWED_N|
|151819026|FOLLOWED_N|
|65845659|FOLLOWED_N|
|190861308|FOLLOWED_N|
|27577743|FOLLOWED_N|

Type denotes the type of the seed, it could be followed by an positive initial account (``FOLLOWED_P``) or be the positive initial account (``INITIAL_P``). The suffix ``N`` indicates that it is a negative example.


#### Initial Seeds
For both, positive and negative, seeds list was handcrafted. In case of economic domain it was a list of journals. A list of the positive accounts can be found [here](accounts.csv), which cointains a description of all found seeds:

|Account|Description|
|-------|-----------|
|Springernomics|Springer Business/Economics/Political Science|
|ELSFinance|Elsevier, Economics & Finance|
|aomconnect|Academy of Managemnt|
|EJ_RES|The Economic Journal / Royal Economic Society|
|WileyEconomics|Wiley|


### Tweets
The file [grabbed_tweets.csv](grabbed_tweets.csv) contains the Twitter IDs of all tweets.  
This file can be found [here](analyzed_tweets.csv) and has the following structure:

|Twitter ID|Tweet from Economist?|
|----------|----|
|22810435083|POSITIVE|
|467448756934565888|POSITIVE|

 An negative ID denotes that the link to an id is omitted; this tweet was not used in the evaluation.